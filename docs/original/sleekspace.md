# Original Sleekspace Theme

The original sleekspace theme was for [Crazy Eddie's GUI (cegui)][cegui] back in 2006 by a contributor names iceey 
(if I remember correctly). You can find the files here:

* [Original Sleekspace Theme][original-sleekspace]

It has since been modified extensively since then, but these are the most recent (but still old) examples of the GUI 
elements:

![sleekspace_highres.png](images%2Fsleekspace_highres.png)

The fonts we used were:

* [GemFontOne](fonts/Gemfont1.ttf)
* [Titillium Web](fonts/titillium.ttf)

This is being preserved here, so it can be used primarily as reference.

[cegui]: http://cegui.org.uk/
[original-sleekspace]: https://gitlab.com/g33xnexus/old-svnrepos/precursors-data/-/tree/master/gui/schemes/sleekspace

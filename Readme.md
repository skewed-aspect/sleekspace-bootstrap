# Sleekspace Bootstrap Theme

This is a simple bootstrap theme for the UI style that we've used in all our RFI games, called 'Sleekspace'.

## Usage

You will want to install both bootstrap and this theme. You can do this with the following commands:

```
$ npm install bootstrap @skewedaspect/sleekspace-bootstrap
```

Then, in your main scss file, you can import the theme:

```scss
@import "@skewedaspect/sleekspace-bootstrap";
```

__Note: You can also import the compiled css file directly, if you don't want to use sass.__

### Color Themes

You will almost certainly want to use this with the `dark` colorscheme, as it's not been designed to work in a light 
environment.

The easiest way to do this is to add `data-bs-theme="dark"` to your `<html>` tag. Example:

```html
<html lang="en" data-bs-theme="dark">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sleekspace Demo</title>
    </head>
    <body>
        <div class="card border-light w-50">
            <div class="card-header bg-dark border-light-subtle">
                Featured
            </div>
            <div class="card-body bg-dark-subtle">
                <h5 class="card-title">Special title treatment</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-outline-light">Go somewhere</a>
            </div>
        </div>
    </body>
</html>
```

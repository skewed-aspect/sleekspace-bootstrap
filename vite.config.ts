//----------------------------------------------------------------------------------------------------------------------

import { resolve } from 'path';
import { defineConfig } from 'vite';

//----------------------------------------------------------------------------------------------------------------------

export default defineConfig({
    root: resolve(__dirname, 'src'),
    publicDir: 'assets',
    build: {
        outDir: '../dist',
        lib: {
            // Could also be a dictionary or array of multiple entry points
            entry: resolve(__dirname, 'src/sleekspace.ts'),
            name: 'SleekSpaceBootstrap',

            // The proper extensions will be added automatically
            fileName: 'sleekspace-bootstrap',
        },
        emptyOutDir: true,
        cssCodeSplit: true
    },
    server: {
        port: 2690
    }
});

//----------------------------------------------------------------------------------------------------------------------
